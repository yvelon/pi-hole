#!/usr/bin/env bash
# Pi-hole: A black hole for Internet advertisements
# (c) 2017 Pi-hole, LLC (https://pi-hole.net)
# Network-wide ad blocking via your own hardware.
#
# Checks for local or remote versions and branches
#
# This file is copyright under the latest version of the EUPL.
# Please see LICENSE file for your rights under this license.

function get_local_branch() {
    # Return active branch
    cd "${1}" 2> /dev/null || return 1
    git rev-parse --abbrev-ref HEAD || return 1
}

function get_local_version() {
    # Return active version
    cd "${1}" 2> /dev/null || return 1
    git describe --tags --always 2> /dev/null || return 1
}

function get_local_hash() {
    cd "${1}" 2> /dev/null || return 1
    git rev-parse --short=8 HEAD || return 1
}

function get_remote_version() {
    curl -s "https://api.github.com/repos/pi-hole/${1}/releases/latest" 2> /dev/null | jq --raw-output .tag_name || return 1
}


function get_remote_hash(){
    git ls-remote "https://github.com/pi-hole/${1}" --tags "${2}" | awk '{print substr($0, 0,8);}' || return 1
}

# Source the setupvars config file for addOrEditKeyValPair()
# shellcheck disable=SC1091
. /etc/pihole/setupVars.conf

# Source the utils file
# shellcheck disable=SC1091
. /opt/pihole/utils.sh

# Remove the below three legacy files if they exist
rm -f "/etc/pihole/GitHubVersions"
rm -f "/etc/pihole/localbranches"
rm -f "/etc/pihole/localversions"

# Create new versions file if it does not exist
VERSION_FILE="/etc/pihole/versions"
touch "${VERSION_FILE}"
chmod 644 "${VERSION_FILE}"


# used in cronjob
if [[ "$1" == "reboot" ]]; then
        sleep 30
fi

# People who had updated may have errors
# Start with a clean slate
echo '' > "${VERSION_FILE}"

# get Core versions
CORE_VERSION="$(get_local_version /etc/.pihole)"
addOrEditKeyValPair "${VERSION_FILE}" "CORE_VERSION" "${CORE_VERSION}"

CORE_BRANCH="$(get_local_branch /etc/.pihole)"
addOrEditKeyValPair "${VERSION_FILE}" "CORE_BRANCH" "${CORE_BRANCH}"

CORE_HASH="$(get_local_hash /etc/.pihole)"
addOrEditKeyValPair "${VERSION_FILE}" "CORE_HASH" "${CORE_HASH}"

GITHUB_CORE_VERSION="$(get_remote_version pi-hole)"
addOrEditKeyValPair "${VERSION_FILE}" "GITHUB_CORE_VERSION" "${GITHUB_CORE_VERSION}"

GITHUB_CORE_HASH="$(get_remote_hash pi-hole "${CORE_BRANCH}")"
addOrEditKeyValPair "${VERSION_FILE}" "GITHUB_CORE_HASH" "${GITHUB_CORE_HASH}"



# get Web versions

if [[ "${INSTALL_WEB_INTERFACE}" == true ]]; then

    WEB_VERSION="$(get_local_version /var/www/html/admin)"
    addOrEditKeyValPair "${VERSION_FILE}" "WEB_VERSION" "${WEB_VERSION}"

    WEB_BRANCH="$(get_local_branch /var/www/html/admin)"
    addOrEditKeyValPair "${VERSION_FILE}" "WEB_BRANCH" "${WEB_BRANCH}"

    WEB_HASH="$(get_local_hash /var/www/html/admin)"
    addOrEditKeyValPair "${VERSION_FILE}" "WEB_HASH" "${WEB_HASH}"

    GITHUB_WEB_VERSION="$(get_remote_version web)"
    addOrEditKeyValPair "${VERSION_FILE}" "GITHUB_WEB_VERSION" "${GITHUB_WEB_VERSION}"

    GITHUB_WEB_HASH="$(get_remote_hash web "${WEB_BRANCH}")"
    addOrEditKeyValPair "${VERSION_FILE}" "GITHUB_WEB_HASH" "${GITHUB_WEB_HASH}"

fi

# get FTL versions
FTL_VERSION="$(pihole-FTL version | cut -d ' ' -f 1)"
addOrEditKeyValPair "${VERSION_FILE}" "FTL_VERSION" "${FTL_VERSION}"

FTL_BRANCH="$(pihole-FTL branch)"
addOrEditKeyValPair "${VERSION_FILE}" "FTL_BRANCH" "${FTL_BRANCH}"

# If FTL has not yet been update and the --hash is not implemented, the versions file
# contains errors that then bash tries to execute
FTL_HASH="$(if pihole-FTL --hash 2>&1 > /dev/null; then pihole-FTL --hash; else echo ''; fi)"
addOrEditKeyValPair "${VERSION_FILE}" "FTL_HASH" "${FTL_HASH}"

GITHUB_FTL_VERSION="$(get_remote_version FTL)"
addOrEditKeyValPair "${VERSION_FILE}" "GITHUB_FTL_VERSION" "${GITHUB_FTL_VERSION}"

GITHUB_FTL_HASH="$(get_remote_hash FTL "${FTL_BRANCH}")"
addOrEditKeyValPair "${VERSION_FILE}" "GITHUB_FTL_HASH" "${GITHUB_FTL_HASH}"
